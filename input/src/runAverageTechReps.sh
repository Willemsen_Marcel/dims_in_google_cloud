#!/bin/bash

echo "### Inputs runAverageTechReps.sh ############################################"
echo PKLIST_DIR:	"${PKLIST_DIR}"
echo PKLIST_DIR_AVG:	"${PKLIST_DIR_AVG}"
echo REPL_PATTERN:	"${REPL_PATTERN}"
echo NREPL:	"${NREPL}"
echo FIT_THRESH:	"${FIT_THRESH}"
echo THRESH2REMOVE:	"${THRESH2REMOVE}"
echo SRC:	"${SRC}"
echo REPL_PATTERN_POS:	"${REPL_PATTERN_POS}"
echo REPL_PATTERN_NEG:	"${REPL_PATTERN_NEG}"
echo MISS_INFUSIONS_POS:	"${MISS_INFUSIONS_POS}"
echo MISS_INFUSIONS_NEG:	"${MISS_INFUSIONS_NEG}"
echo "#############################################################################"

echo "Run file $file in R"
echo "`pwd`"

R --slave --no-save --no-restore --no-environ --args "${PKLIST_DIR}" "${PKLIST_DIR_AVG}" "${REPL_PATTERN}" "${NREPL}" "${FIT_THRESH}" "${THRESH2REMOVE}" "${REPL_PATTERN_POS}" "${REPL_PATTERN_NEG}" "${MISS_INFUSIONS_POS}" "${MISS_INFUSIONS_NEG}"< "${RSCRIPT}"