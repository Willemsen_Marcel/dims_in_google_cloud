#!/bin/bash

echo "### Inputs runDims.sh #############################################"
#echo OUTDIR: "${OUTDIR}"
echo SRC: "${SRC}"
echo INPUT_BREAKS: "${INPUT_BREAKS}"
echo INPUT_mzXML: "${INPUT_mzXML}"
echo TRIM: "${TRIM}"
echo FIT_THRESH: "${FIT_THRESH}"
echo RSCRIPT: "${RSCRIPT}"
echo RESOL: "${RESOL}"
echo OUTPUT_FILES: "${OUTPUT_FILES}"
#echo Working dir: `pwd` 
echo "#############################################################################"

R --slave --no-save --no-restore --no-environ --args ${INPUT_mzXML} ${SRC} ${INPUT_BREAKS} ${TRIM} ${FIT_THRESH} ${RESOL} ${OUTPUT_FILES} < ${RSCRIPT}