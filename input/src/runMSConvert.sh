#!/bin/bash

echo "### Inputs runMSConvert.sh ##################################################"
echo INPUT_FILES: "${INPUT_FILES}"
echo OUTPUTPATH: "${OUTPUTPATH}"
echo Working dir: `pwd` 
echo "#############################################################################"

wine msconvert ${INPUT_FILES} -o ${OUTPUTPATH} --mzXML