#!/bin/bash

echo "### Inputs runGenerateBreaks.sh #############################################"
echo INDIR: "${INDIR}"
echo OUTDIR: "${OUTDIR}"
echo FILE: "${FILE}"
echo REPL_PATTERN: "${REPL_PATTERN}"
echo RSCRIPT: "${RSCRIPT}"
echo BREAKS_FWHM: "${BREAKS_FWHM}"
echo SAMPLE_LIST: "${SAMPLE_LIST}"
#echo Working dir: `pwd` 
echo "#############################################################################"

R --slave --no-save --no-restore --no-environ --args ${FILE} ${BREAKS_FWHM} ${REPL_PATTERN} ${SAMPLE_LIST} ${INDIR} ${OUTDIR} < "${RSCRIPT}"