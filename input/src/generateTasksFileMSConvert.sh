#!/bin/bash

echo '--input INPUT_FILES' >> "${OUTPUT_TASKS_FILE}"

find "${INPUT_DIR}" -iname "*.raw" | while read rdata;
 do

     file_out="";
     it=0
     for i in $(echo $rdata | tr "/" "\n")
      do
      
        it=$((it+1))

        if [ "${it}" -eq "${BUCKET_PATH_STEP}" ]; then
          file_out="${i}"
          echo "${file_out}"
        fi
        
      done
     
     echo -e "${IN_DIR}/${file_out}" >> "${OUTPUT_TASKS_FILE}"
 done
