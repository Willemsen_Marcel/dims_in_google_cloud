### fit Gaussian estimate mean and integrate to obtain intensity
findPeaks.Gauss.HPC <- function(plist, breaks_fwhm, int_factor, scale, resol, outdir, sampname, scanmode, plot, thresh, width, height) {
  # plist=sum_neg
  # plot=FALSE

  # breaks_fwhm=breaks_fwhm
  # int_factor=int_factor
  # scale=scale
  # resol=resol
  # outdir=outdir
  # sampname=sampname
  # scanmode=scanmode
  # thresh=thresh
  # width=width
  # height=height 

  range = as.vector(plist)
  names(range) = rownames(plist)
  #range[34:43]

  values = list("mean"=NULL,"area"=NULL,"nr"=NULL,"min"=NULL,"max"=NULL,"qual"=NULL,"spikes"=0)
  
  values = searchMZRange(range,values,int_factor,scale,resol,outdir,sampname,scanmode,plot,width,height,thresh)  

  outlist_persample=NULL
  outlist_persample=cbind("samplenr"=values$nr, "mzmed.pkt"=values$mean, "fq"=values$qual, "mzmin.pkt"=values$min, "mzmax.pkt"=values$max, "height.pkt"=values$area)
  #outlist_persample=cbind("samplenr"=sample.nr, "mzmed.pkt"=peak.mean, "fq"=peak.qual, "mzmin.pkt"=peak.min, "mzmax.pkt"=peak.max, "height.pkt"=peak.area)
  index=which(outlist_persample[,"height.pkt"]==0)
  if (length(index)>0){
    outlist_persample=outlist_persample[-index,]  
  }

  # save(outlist_persample, file=paste(outdir, paste(sampname, "_", scanmode, ".RData", sep=""), sep="/"))
  save(outlist_persample, file=outdir)
  
  message(paste("There were", values$spikes, "spikes!"))
  #return(peaklist.all)
}  
