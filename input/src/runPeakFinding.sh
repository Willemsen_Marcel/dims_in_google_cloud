#!/bin/bash

echo "### Inputs runPeakFinding.sh #####################################################"
echo INPUT_FILES: "${INPUT_FILES}"
echo SRC: "${SRC}"
echo OUTPUT_FILES: "${OUTPUT_FILES}"
echo THRESH: "${THRESH}"
echo RESOL: "${RESOL}"
echo RSCRIPT: "${RSCRIPT}"
echo "#############################################################################"

echo "Run file ${INPUT_FILES} in R"
echo "`pwd`"

R --slave --no-save --no-restore --no-environ --args "${INPUT_FILES}" "${SRC}" "${OUTPUT_FILES}" "${THRESH}" "${RESOL}" "${INPUT_BREAKS}" < "${RSCRIPT}"