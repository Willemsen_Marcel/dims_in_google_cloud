#!/bin/bash

#################################################################################
##################### Google Cloud Pipline ######################################
#################################################################################
# Author: Marcel Willemsen, a.m.willemsen.marcel@gmail.com, 0614455321 #########
#################################################################################
#################################################################################

#################################################################################
############### start Python virtual environment ################################
#################################################################################
# python3 -m venv dsub_libs
# source dsub_libs/bin/activate
# deactivate

 # dsub \
 #   --provider local \
 #   --logging "${TMPDIR:-/tmp}/dsub-test/logging/" \
 #   --output OUT="${TMPDIR:-/tmp}/dsub-test/output/out.txt" \
 #   --command 'echo "Hello World" > "${OUT}"' \
 #   --wait

#################################################################################

set -o errexit
set -o nounset

#################################################################################
############### provider: local #################################################
#################################################################################
rm -Rf tmp
mkdir tmp
export TMPDIR=tmp

readonly MY_BUCKET_PATH="."
readonly PROVIDER=local
readonly DISK_SIZE=20
readonly LOGGING="${MY_BUCKET_PATH}"/tmp/dsub-test
readonly BUCKET_PATH_STEP=7
#################################################################################

# #################################################################################
# ############### provider: Google ################################################
# #################################################################################
# readonly MY_BUCKET_PATH="gs://marcel-willemsen-storage-bucket"
# readonly PROVIDER=google-cls-v2
# readonly DISK_SIZE=200
# readonly LOGGING="${MY_BUCKET_PATH}"
# readonly BUCKET_PATH_STEP=8
# #################################################################################

readonly MY_PROJECT="dsub-test-242710"
readonly ZONES="us-central1-*"
readonly OUTDIR="${MY_BUCKET_PATH}"/output
readonly INDIR="${MY_BUCKET_PATH}"/input

#################################################################################
# Logging start

RSCRIPT="${INDIR}"/src/start.R

dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "start" \
  --image bioconductor/release_mscore2 \
  --input RSCRIPT="${RSCRIPT}" \
  --command 'R --slave --no-save --no-restore --no-environ < "${RSCRIPT}"'

# dstat --provider local --jobs 'start--marcel--*' --users 'marcel' --status '*'  
#################################################################################

# Job properties:
#   job-id: start--marcel--210907-144657-89
#   job-name: start
#   user-id: marcel
# Launched job-id: start--marcel--210907-144657-89
# To check the status, run:
# dstat --provider local --jobs 'peakfind--marcel--211006-164120-21' --users 'marcel' --status '*'
# dstat --provider local --users 'marcel' --status '*'
# To cancel the job, run:
#   ddel --provider local --jobs 'start--marcel--210907-144657-89' --users 'marcel'
# start--marcel--210907-144657-89

#################################################################################
# createTaskFile

OUTPUT_TASKS_FILE="${OUTDIR}"/sample_list_msconvert.tsv
INPUT_DIR="${INDIR}"/raw

TASKSFILE_ID=$(dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "tasksfile" \
  --input-recursive INPUT_DIR="${INPUT_DIR}" \
  --output OUTPUT_TASKS_FILE="${OUTPUT_TASKS_FILE}" \
  --env IN_DIR="${INPUT_DIR}" BUCKET_PATH_STEP="${BUCKET_PATH_STEP}" \
  --image bioconductor/release_mscore2 \
  --script "${INDIR}"/src/generateTasksFileMSConvert.sh | grep tasksfile--marcel--*)
  # --wait
#################################################################################

echo "==============> Job ${TASKSFILE_ID}"

#################################################################################
# Raw file conversion

# writing output file: /mnt/data/output/gs/marcel-willemsen-storage-bucket/output/data\RES_DBS_20180528_007.mzXML
#
# 2019-06-18 10:48:31 INFO: Delocalizing OUTPUTPATH
# 2019-06-18 10:48:31 INFO: gsutil  -mq rsync -r /mnt/data/output/gs/marcel-willemsen-storage-bucket/output/data/ gs://marcel-willemsen-storage-bucket/output/data/

CONVERSION_ID=$(dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "conversion" \
  --after "${TASKSFILE_ID}" \
  --output-recursive OUTPUTPATH="${OUTDIR}"/data \
  --image chambm/pwiz-skyline-i-agree-to-the-vendor-licenses \
  --tasks "${OUTPUT_TASKS_FILE}" \
  --script "${INDIR}"/src/runMSConvert.sh | grep conversion--marcel--*)
  # --wait
#################################################################################

echo "==============> Job ${CONVERSION_ID}"

#################################################################################
# Generate breaks

RSCRIPT="${INDIR}"/src/generateBreaksFwhm.HPC.R

readonly BREAKS_FWHM="${OUTDIR}"/breaks.fwhm.RData
readonly SAMPLE_LIST_DIMS="${OUTDIR}"/sample_list_dims.tsv

readonly FILE="${OUTDIR}"/data/RES_DBS_20180528_001.mzXML #ToDo automate! One input file needed.
readonly REPL_PATTERN="${INDIR}"/repl_pattern.RData

BREAKS_ID=$(dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "breaks" \
  --after "${CONVERSION_ID}" \
  --input RSCRIPT="${RSCRIPT}" FILE="${FILE}" REPL_PATTERN="${REPL_PATTERN}" \
  --output BREAKS_FWHM="${BREAKS_FWHM}" SAMPLE_LIST="${SAMPLE_LIST_DIMS}" \
  --env OUTDIR="${OUTDIR}" INDIR="${INDIR}" \
  --image bioconductor/release_mscore2 \
  --script "${INDIR}"/src/runGenerateBreaks.sh | grep breaks--marcel--*) 
  # --wait
###############################################################################

echo "==============> Job ${BREAKS_ID}"

###############################################################################
# Dims

RSCRIPT="${INDIR}"/src/DIMS.R

readonly INPUT_BREAKS="${OUTDIR}"/breaks.fwhm.RData

readonly SRC="${INDIR}"/src/AddOnFunctions

readonly TRIM=0.1
readonly FIT_THRESH=100
readonly RESOL=140000

DIMS_ID=$(dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "dims" \
  --after "${BREAKS_ID}" \
  --input RSCRIPT="${RSCRIPT}" INPUT_BREAKS="${INPUT_BREAKS}" \
  --input-recursive SRC="${SRC}" \
  --env TRIM="${TRIM}" FIT_THRESH="${FIT_THRESH}" RESOL="${RESOL}" \
  --image bioconductor/release_mscore2 \
  --tasks "${SAMPLE_LIST_DIMS}" \
  --script "${INDIR}"/src/runDims.sh | grep dims--marcel--*)
  # --wait
###############################################################################

echo "==============> Job ${DIMS_ID}"

###############################################################################
# AverageTechReps

RSCRIPT="${INDIR}"/src/averageTechReplicates.R

readonly PKLIST_DIR="${OUTDIR}"/pklist
readonly PKLIST_DIR_AVG="${OUTDIR}"/average_pklist
readonly REPL_PATTERN_POS="${OUTDIR}"/repl.pattern.positive.RData
readonly REPL_PATTERN_NEG="${OUTDIR}"/repl.pattern.negative.RData
readonly MISS_INFUSIONS_POS="${OUTDIR}"/miss_infusions_pos.txt
readonly MISS_INFUSIONS_NEG="${OUTDIR}"/miss_infusions_neg.txt

#thresh2remove = 1*10^9 # plasma
readonly THRESH2REMOVE=500000000 # 5*10^8 # DBS
# thresh2remove = 1*10^8 # research (Mia)
readonly NREPL=3

AVERAGE_ID=$(dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "average" \
  --after "${DIMS_ID}" \
  --input RSCRIPT="${RSCRIPT}" REPL_PATTERN="${REPL_PATTERN}" \
  --input-recursive SRC="${SRC}" PKLIST_DIR="${PKLIST_DIR}" \
  --output REPL_PATTERN_POS="${REPL_PATTERN_POS}" REPL_PATTERN_NEG="${REPL_PATTERN_NEG}" MISS_INFUSIONS_POS="${MISS_INFUSIONS_POS}" MISS_INFUSIONS_NEG="${MISS_INFUSIONS_NEG}" \
  --output-recursive PKLIST_DIR_AVG="${PKLIST_DIR_AVG}" \
  --env NREPL="${NREPL}" THRESH2REMOVE="${THRESH2REMOVE}" FIT_THRESH="${FIT_THRESH}" \
  --image bioconductor/release_mscore2 \
  --script "${INDIR}"/src/runAverageTechReps.sh | grep average--marcel--*)
  #\
  #--wait
# dstat --provider local --jobs 'average--marcel--210907-160114-96' --users 'marcel' --status '*'
###############################################################################
 
echo "==============> Job ${AVERAGE_ID}"

###############################################################################
# createTaskFile

OUTPUT_TASKS_FILE="${OUTDIR}"/sample_list_peakfinding.tsv
readonly INPUT_DIR="${OUTDIR}"/average_pklist
readonly OUTPUT_DIR="${OUTDIR}"/specpks

TASKSFILE2_ID=$(dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "tasksfile2" \
  --after "${AVERAGE_ID}" \
  --input-recursive INPUT_DIR="${INPUT_DIR}" \
  --output OUTPUT_TASKS_FILE="${OUTPUT_TASKS_FILE}" \
  --env OUTPUT_DIR="${OUTPUT_DIR}" IN_DIR="${INPUT_DIR}" BUCKET_PATH_STEP="${BUCKET_PATH_STEP}" \
  --image bioconductor/release_mscore2 \
  --script "${INDIR}"/src/generateTasksFile.sh | grep tasksfile2--marcel--*) 
  # --wait
###############################################################################

echo "==============> Job ${TASKSFILE2_ID}"

# ==============> Job tasksfile2--marcel--211007-113957-87
# Traceback (most recent call last):
#   File "/home/marcel/R-workspace/dims_in_google_cloud/dsub_libs/bin/dsub", line 11, in <module>
#     load_entry_point('dsub==0.4.5', 'console_scripts', 'dsub')()
#   File "/home/marcel/environments/dsub_libs/lib/python3.8/site-packages/dsub/commands/dsub.py", line 1115, in main
#     dsub_main(prog, argv)
#   File "/home/marcel/environments/dsub_libs/lib/python3.8/site-packages/dsub/commands/dsub.py", line 1100, in dsub_main
#     launched_job = run_main(args)
#   File "/home/marcel/environments/dsub_libs/lib/python3.8/site-packages/dsub/commands/dsub.py", line 1150, in run_main
#     task_descriptors = param_util.tasks_file_to_task_descriptors(
#   File "/home/marcel/environments/dsub_libs/lib/python3.8/site-packages/dsub/lib/param_util.py", line 515, in tasks_file_to_task_descriptors
#     param_file = dsub_util.load_file(path)
#   File "/home/marcel/environments/dsub_libs/lib/python3.8/site-packages/dsub/lib/dsub_util.py", line 212, in load_file
#     with open(file_path, 'r') as f:
# FileNotFoundError: [Errno 2] No such file or directory: './output/sample_list_peakfinding.tsv'  ==> is er wel!! I/O duurt te lang?

###############################################################################
# PeakFinding

RSCRIPT="${INDIR}"/src/peakFinding.2.0.R
readonly THRESH=2000

dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "peakfind" \
  --after "${TASKSFILE2_ID}" \
  --input RSCRIPT="${RSCRIPT}" INPUT_BREAKS="${INPUT_BREAKS}" \
  --input-recursive SRC="${SRC}" \
  --env THRESH="${THRESH}" RESOL="${RESOL}" \
  --image bioconductor/release_mscore2 \
  --tasks "${OUTPUT_TASKS_FILE}" \
  --script "${INDIR}"/src/runPeakFinding.sh
  # \
  # --wait
###############################################################################

# ###############################################################################
# # Logging stop
# 
# RSCRIPT="${INDIR}"/src/stop.R
# 
# dsub \
#   --provider "${PROVIDER}" \
#   --project "${MY_PROJECT}" \
#   --zones "${ZONES}" \
#   --logging "${LOGGING}"/logging \
#   --name "stop" \
#   --image bioconductor/release_mscore2 \
#   --input RSCRIPT="${RSCRIPT}" \
#   --command 'R --slave --no-save --no-restore --no-environ < ${RSCRIPT}' \
# ###############################################################################
