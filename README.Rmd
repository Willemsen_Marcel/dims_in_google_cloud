
---
title: "Get a kickstart to setup your bioinformatics pipeline in the cloud"
author: "a.m.willemsen.marcel@gmail.com"
date: "2019-07-02"
output:
  html_document:
    highlight: pygments
---

![](google_cloud_2.png)

```{r figurename, echo=FALSE, fig.cap="", out.width = '50%', eval = FALSE}
knitr::include_graphics("google_cloud_2.png")
```

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Introduction

Recently I migrated a high-throughput untargeted metabolomics pipeline form a high performance computing (HPC) facility to _Google Cloud Platform_ (GCP). In this tutorial I like to share my experiences and walk you step by step through the process.

When it comes to cloud computing there are several players on the market so the first thing to do is choose your cloud provider. I chose Google because they charge you only for what you use (computing time and storage) which makes it relatively cheap. Besides, you receive $300 dollar free trial.

There are several ways you could implement a pipeline on GCP (see also link). I chose to use _dsub_ because the user experience is a lot like traditional high-performance computing job schedulers like _Grid Engine_ which I’m used to work with. Furthermore, it has the additional benefit that the code wouldn’t have to be changed to much. The only code that needed to be changed was the bash scripts that submit the batch jobs. The R code could be kept unchanged.

Another advantage of dsub is that you can run your test scripts on your local machine in the same way as it runs on the backend batch job runner, Google cloud in this case. You write a script and then submit it to a job scheduler from a shell prompt on your local machine you can choose if you use the local provider (your own laptop) or a cloud provider. This will save you money during the test phase.

Batch jobs with Docker = flexible switch between operating systems!!!

At the same time a local script that submit jobs has the drawback that, although the heavy computing is performed in the cloud, there is still something running on your local machine. Executing consecutive jobs that have to wait on each other, which I believe is almost always the case with bioinformatics pipelines, can take some time during which you can’t turn off your machine. Maybe running this script in the cloud in a Docker container with an image that has dsub installed might serve a solution for this in a production environment. 

## Pipeline (first steps)

To understand what is going on some basic knowledge about the pipeline is necessary. The concerning pipeline is used to pre-process and analyse raw data from untargeted direct infusion mass spectrometry (DI-MS). Since my goal here is to discuss GCP and dsub and not the pipeline itself, I will only consider the first 6 steps of this pipeline which should cover all basic proceedings, the rest is more of the same. The first six pipeline steps are:

* Loggin a timestamp
  * To be able to check how long the pipeline took afterwards 
* Creating a task file
  * By providing a task file with input and output parameters, each line in this file will be executed in parallel (like an array job in qsub/Grid Engine) 
* Raw file conversion
  * All raw input files have to be converted to mzXML files. This step uses the input file created 	above.
* Generating breaks and task file
  * The full with at half maximum (FWHM) of a MS peak is increasing (not lineair) with the mass over change (m/z). In the following steps (e.g. peak finding, not included here) we need a vector indicating break points, the bins, proportional with the FWHM. This vector can be used for all samples. A task file for the next step is created.
* Aggregation
  * Signal in each bin is collected	
* Averaging technical replicates
* Creating a task file
  * Sometimes it is easier to create a task file for the next step based on the output files of the previous step   
* ... 
  * Etc.
* Loggin a timestamp
  * Again, to be able to check how long the pipeline took afterwards

## Data set

Most DI-MS experiments have a case vs. control design. For this purpose I created a simplified data set with only 3 biological case samples (actually only one copied several times) each having 3 technical replicates, thus 9 samples in total. This design is saved in a R object called “repl_pattern.Rdata” which is a list of 3 vectors each containing 3 raw file labels. The labels correspond with the raw file names (without extension). This object file together with the raw data files form the mass spectrometer is the input of the pipeline. The output will be ...

```{r, echo=FALSE}
load("./input/repl_pattern.RData")
```

Experimental design:
```{r, echo=FALSE}
repl_pattern
```

## Installation

For now I hope you have enough background information to get started. First you have to install dsub and Google Cloud SDK. This [README](https://github.com/DataBiosphere/dsub) describes step by step how to. When dsub is installed you will have a nice examples directory which is worth looking at. To make the local example work (under Ubuntu 18.04) I had to add a work around by creating a alternative temp directory under my working dir:

```{r, engine = 'bash', eval = FALSE}
mkdir tmp
export TMPDIR=tmp
```

You have to remove this temp dir after each test run!

## Getting started

Now you are ready to write your first dsub script (in bash). The code for this pipeline can be downloaded from GitHub. The bash script for this pipeline is in the root of the GitHup project and is called “Untargeted_DIMS_pipeline.sh”.  I will now walk step by step through this script.

Activate the dsub environment:

```{r, engine = 'bash', eval = FALSE}
source ./dsub_libs/bin/activate
```

In the first 2 sections some constants are declared which vary depending on the provider you use, one of them should be commented out. My advise is to test your scripts first on your local machine with only two samples and when everything works fine run it in the cloud:

```{r, engine = 'bash', eval = FALSE}
#################################################################################
############### provider: local #################################################
#################################################################################
readonly MY_BUCKET_PATH="."
readonly PROVIDER=local
readonly DISK_SIZE=20
readonly LOGGING="${MY_BUCKET_PATH}"/tmp/dsub-test
readonly BUCKET_PATH_STEP=7
#################################################################################

# #################################################################################
# ############### provider: Google ################################################
# #################################################################################
# readonly MY_BUCKET_PATH="gs://your-storage-bucket"
# readonly PROVIDER=google-v2
# readonly DISK_SIZE=200
# readonly LOGGING="${MY_BUCKET_PATH}"
# readonly BUCKET_PATH_STEP=8
# #################################################################################

readonly MY_PROJECT="your-project-id"
readonly ZONES="us-central1-*"
readonly OUTDIR="${MY_BUCKET_PATH}"/output
readonly INDIR="${MY_BUCKET_PATH}"/input
```

When using Google as provider you should change _MY_BUCKET_PATH_ with the name of yours. The _BUCKET_PATH_STEP_ is the number off sub directories in the path from the root to the place where the raw input files are stored in the temp dir. On excecution the input files are copied from _./input/raw_ to the temp directory which simulates the node in the cloud where the batch jobs will be excecuted. In the cloud this is one more then in the local situation. Not sure if this is always the case.     

```{r, engine = 'bash', eval = FALSE}
#################################################################################
# Logging start

RSCRIPT="${INDIR}"/src/start.R

dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "start" \
  --image bioconductor/release_mscore2 \
  --input RSCRIPT="${RSCRIPT}" \
  --command "R --slave --no-save --no-restore --no-environ < ${RSCRIPT}"
#################################################################################
```

```{r, eval = FALSE}
message("##########################################################")  
message(paste("Pipeline start at:", Sys.time()))  
message("##########################################################")  
```


```{r, engine = 'bash', eval = FALSE}
#################################################################################
# Create task file

OUTPUT_TASKS_FILE="${OUTDIR}"/sample_list_msconvert.tsv
INPUT_DIR="${INDIR}"/raw

dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "tasksfile" \
  --input-recursive INPUT_DIR="${INPUT_DIR}" \
  --output OUTPUT_TASKS_FILE="${OUTPUT_TASKS_FILE}" \
  --env IN_DIR="${INPUT_DIR}" BUCKET_PATH_STEP="${BUCKET_PATH_STEP}" \
  --image bioconductor/release_mscore2 \
  --script "${INDIR}"/src/generateTasksFileMSConvert.sh \
  --wait
#################################################################################
```

```{r, engine = 'bash', eval = FALSE}
#!/bin/bash

echo '--input INPUT_FILES' >> "${OUTPUT_TASKS_FILE}"

find "${INPUT_DIR}" -iname "*.raw" | while read rdata;
 do

     file_out="";
     it=0
     for i in $(echo $rdata | tr "/" "\n")
      do
      
        it=$((it+1))

        if [ "${it}" -eq "${BUCKET_PATH_STEP}" ]; then
          file_out="${i}"
          echo "${file_out}"
        fi
        
      done
     
     echo -e "${IN_DIR}/${file_out}" >> "${OUTPUT_TASKS_FILE}"
 done
```


```{r, engine = 'bash', eval = FALSE}
#################################################################################
# Raw file conversion

dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "conversion" \
  --output-recursive OUTPUTPATH="${OUTDIR}"/data \
  --image chambm/pwiz-skyline-i-agree-to-the-vendor-licenses \
  --tasks "${OUTPUT_TASKS_FILE}" \
  --script "${INDIR}"/src/runMSConvert.sh \
  --wait
#################################################################################
```

```{r, engine = 'bash', eval = FALSE}
#################################################################################
# Generate breaks

RSCRIPT="${INDIR}"/src/generateBreaksFwhm.HPC.R

readonly BREAKS_FWHM="${OUTDIR}"/breaks.fwhm.RData
readonly SAMPLE_LIST_DIMS="${OUTDIR}"/sample_list_dims.tsv

readonly FILE="${OUTDIR}"/data/RES_DBS_20180528_001.mzXML #ToDo automate! One input file needed.
readonly REPL_PATTERN="${INDIR}"/repl_pattern.RData

dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "breaks" \
  --input RSCRIPT="${RSCRIPT}" FILE="${FILE}" REPL_PATTERN="${REPL_PATTERN}" \
  --output BREAKS_FWHM="${BREAKS_FWHM}" SAMPLE_LIST="${SAMPLE_LIST_DIMS}" \
  --env OUTDIR="${OUTDIR}" INDIR="${INDIR}" \
  --image bioconductor/release_mscore2 \
  --script "${INDIR}"/src/runGenerateBreaks.sh \
  --wait
#################################################################################
```

```{r, engine = 'bash', eval = FALSE}
#################################################################################
# Aggregation

RSCRIPT="${INDIR}"/src/DIMS.R

readonly INPUT_BREAKS="${OUTDIR}"/breaks.fwhm.RData

readonly SRC="${INDIR}"/src/AddOnFunctions

readonly TRIM=0.1
readonly FIT_THRESH=100
readonly RESOL=140000

dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "dims" \
  --input RSCRIPT="${RSCRIPT}" INPUT_BREAKS="${INPUT_BREAKS}" \
  --input-recursive SRC="${SRC}" \
  --env TRIM="${TRIM}" FIT_THRESH="${FIT_THRESH}" RESOL="${RESOL}" \
  --image bioconductor/release_mscore2 \
  --tasks "${SAMPLE_LIST_DIMS}" \
  --script "${INDIR}"/src/runDims.sh \
  --wait
#################################################################################
```

```{r, engine = 'bash', eval = FALSE}
#################################################################################
# Averaging technical replicates

RSCRIPT="${INDIR}"/src/averageTechReplicates.R

readonly PKLIST_DIR="${OUTDIR}"/pklist
readonly PKLIST_DIR_AVG="${OUTDIR}"/average_pklist
readonly REPL_PATTERN_POS="${OUTDIR}"/repl.pattern.positive.RData
readonly REPL_PATTERN_NEG="${OUTDIR}"/repl.pattern.negative.RData
readonly MISS_INFUSIONS_POS="${OUTDIR}"/miss_infusions_pos.txt
readonly MISS_INFUSIONS_NEG="${OUTDIR}"/miss_infusions_neg.txt

readonly THRESH2REMOVE=500000000
readonly NREPL=3

dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "average" \
  --input RSCRIPT="${RSCRIPT}" REPL_PATTERN="${REPL_PATTERN}" \
  --input-recursive SRC="${SRC}" PKLIST_DIR="${PKLIST_DIR}" \
  --output REPL_PATTERN_POS="${REPL_PATTERN_POS}" REPL_PATTERN_NEG="${REPL_PATTERN_NEG}" MISS_INFUSIONS_POS="${MISS_INFUSIONS_POS}" MISS_INFUSIONS_NEG="${MISS_INFUSIONS_NEG}" \
  --output-recursive PKLIST_DIR_AVG="${PKLIST_DIR_AVG}" \
  --env NREPL="${NREPL}" THRESH2REMOVE="${THRESH2REMOVE}" FIT_THRESH="${FIT_THRESH}" \
  --image bioconductor/release_mscore2 \
  --script "${INDIR}"/src/runAverageTechReps.sh \
  --wait
#################################################################################
```

```{r, engine = 'bash', eval = FALSE}
#################################################################################
# Create task file

OUTPUT_TASKS_FILE="${OUTDIR}"/sample_list_peakfinding.tsv
readonly INPUT_DIR="${OUTDIR}"/average_pklist
readonly OUTPUT_DIR="${OUTDIR}"/specpks

dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "tasksfile" \
  --input-recursive INPUT_DIR="${INPUT_DIR}" \
  --output OUTPUT_TASKS_FILE="${OUTPUT_TASKS_FILE}" \
  --env OUTPUT_DIR="${OUTPUT_DIR}" IN_DIR="${INPUT_DIR}" BUCKET_PATH_STEP="${BUCKET_PATH_STEP}" \
  --image bioconductor/release_mscore2 \
  --script "${INDIR}"/src/generateTasksFile.sh \
  --wait
#################################################################################
```

```{r, engine = 'bash', eval =FALSE}
"... do some more consecutive pathway steps ..."
```

```{r, engine = 'bash', eval = FALSE}
###############################################################################
# Logging stop

RSCRIPT="${INDIR}"/src/stop.R

dsub \
  --provider "${PROVIDER}" \
  --project "${MY_PROJECT}" \
  --zones "${ZONES}" \
  --logging "${LOGGING}"/logging \
  --name "stop" \
  --image bioconductor/release_mscore2 \
  --input RSCRIPT="${RSCRIPT}" \
  --command "R --slave --no-save --no-restore --no-environ < ${RSCRIPT}"
###############################################################################
```
